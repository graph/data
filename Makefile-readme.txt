# 1/ in snap/ : Undirected graphs from Stanford Snap database
# and 2/ in massive/ : graphs mentionned in Delling, Goldberg, Werneck paper in ESA'14.


# 1/ -------------- Stanford Snap database :

SNAP:=snap

# com-friendster.ungraph.txt.gz was skipped (1 GB)
# com-orkut.ungraph.txt.gz was skipped (430 MB)
# com-lj.ungraph.txt.gz was skipped (110 MB)

SNAP_DATA:=http://snap.stanford.edu/data

$(SNAP)/index.html:
        mkdir -p $(SNAP)
        curl -o $(SNAP)/index.html $(SNAP_DATA)/index.html

$(SNAP)/index.txt: $(SNAP)/index.html
        cat _data/snap/index.html | tr -d "\012\015" |  sed -E -e 's|<!--([^-]*(-[^-]+)*(--[^>]+)*)*-->||g' | sed -e 's/<tr>/@b-tr-/g' -e 's|</tr>|e-tr-@|g' | tr "@" "\012" | grep -e '-tr-' | grep -e '<td>\(Directed\|Undirected\)' | sed -e 's|[^"]*"\(.*\).html".*<td>\(.*\)rected.*|\2rected/\1|' > $@

$(SNAP)/%.txt.gz: $(SNAP)/%.html
        f=$$(basename $@) ;\
        make $@.mkdir ;\
        curl -o $@ $(SNAP_DATA)/$$f ;\
        if [ -n "$(SLEEP)" ] ; then sleep $(SLEEP) ; fi ;\
        if head $@ | grep -q '404 Not Found' ; then\
                mv -f $@ $@.error ;\
        fi

$(SNAP)/%.html:
        f=$$(basename $@) ;\
        make $@.mkdir ;\
        curl -o $@ $(SNAP_DATA)/$$f ;\
        if [ -n "$(SLEEP)" ] ; then sleep $(SLEEP) ; fi ;\
        if head $@ | grep -q '404 Not Found' ; then\
                mv -f $@ $@.error ;\
        fi

.PRECIOUS:$(SNAP)/%.html

$(SNAP)/%.getgraph: $(SNAP)/%.html
        first=$$(cat $< | tr -d "\012\015" | sed -e 's/.*"Table of datasets"//'  -e 's/[^"]*"\([^"]*\)"/\1@/g' -e 's/\(.*\)@.*/\1/' -e 's/\.txt\.gz@.*/.txt.gz/' -e 's/^.*@//') ;\
        echo "First file, $$first" > $(SNAP)/$*.stats ;\
        cat $< | tr -d "\012\015" | sed -e 's/.*Dataset statistics//' -e 's|</table>.*||' -e 's|</td> *<td>|, |g'  -e 's/ *<[^>]*> */@/g' -e 's/@@*/@/g' -e 's/^@//' -e 's/@$$//' | tr "@" "\012" >> $(SNAP)/$*.stats ;\
        file=$(SNAP_DATA)/$$first ;\
        dest=`dirname $<`/`basename $$file` ;\
        if [ ! -f $$dest ] ; then curl -o $$dest $$file ; fi ;\
        if [ ! -f $$dest ] ; then touch $<.error ;\
        elif head $$dest | grep -q '404 Not Found' ; then\
                mv -f $$dest $<.error ;\
        fi
        if [ -n "$(SLEEP)" ] ; then sleep $(SLEEP) ; fi

snap_download: $(SNAP)/index.txt
        for p in $$(cat $^) ; do\
                make $(SNAP)/$$p.html SLEEP=60 ;\
                if [ -f $(SNAP)/$$p.html ] ; then\
                        make $(SNAP)/$$p.getgraph SLEEP=60 ;\
                else touch $(SNAP)/$$p.error ;\
                fi ;\
        done



# 2/ ---------- Graphs used in :
# Robust Exact Distance Queries on Massive Networks
# Delling, Goldberg, Werneck
# ESA'14

# The data is available from snap.stanford.edu, webgraph.di.unimi.it, www.dis.un
iroma1.it/challenge9, and socialnetworks.mpi-sws.org/ datasets.html. We also tes
t unweighted grid graphs with holes from VLSI applications (alue7065; steinlib.z
ib.de) and grids with obstacles built from computer games (FrozenSea, AR0503SR; 
movingai.com).

MASS:=massive

massive: webgraph_data dimacs9 vlsi wargrids buddha

# skipped indochina-2004-d.txt.gz (450 MB)
# skipped hollywood-2009.txt.gz (305 MB)


# http://data.law.di.unimi.it/webdata/cnr-2000/cnr-2000-hc.graph

webgraph_data:
        mkdir -p _data/inputWebgraph/Directed _data/inputAscii/Directed
        for f in indochina-2004 ; do \
                curl -o $(MASS)/$$f.graph http://data.law.di.unimi.it/webdata/$$f/$$f.graph ; sleep 1; \
                curl -o $(MASS)/$$f.properties http://data.law.di.unimi.it/webdata/$$f/$$f.properties ; sleep 1; \
                cd _data/inputWebgraph/Directed/; ln -sf ../../massive/$$f.* .; cd ../../..;\
                cd _data; ../_jdk1.8.0_144/bin/java -jar ../OffsetsBVGraph.jar -o -O inputWebgraph/Directed/$$f; cd ..;\
        done
        mkdir -p _data/inputWebgraph/Undirected _data/inputAscii/Undirected
        for f in hollywood-2009 dblp-2010 ; do \
                curl -o $(MASS)/$$f.graph http://data.law.di.unimi.it/webdata/$$f/$$f.graph ; sleep 1; \
                curl -o $(MASS)/$$f.properties http://data.law.di.unimi.it/webdata/$$f/$$f.properties ; sleep 1; \
                cd _data/inputWebgraph/Undirected/; ln -sf ../../massive/$$f.* .; cd ../../..;\
                cd _data; ../_jdk1.8.0_144/bin/java -jar ../OffsetsBVGraph.jar -o -O inputWebgraph/Undirected/$$f; cd ..;\
        done
        cd _data; ../_jdk1.8.0_144/bin/java  -Xmx2g -jar ../SmSwp.jar -p convertascii || echo "let's try anyway"
        rm -f _data/inputAscii/*irected/*.txt.gz
        gzip _data/inputAscii/*irected/*.txt
        cd $(MASS) ; for f in ../inputAscii/Directed/*.txt.gz; do ln -sf $$f `basename $$f .txt.gz`-d.txt.gz ; done
        cd $(MASS) ; for f in ../inputAscii/Undirected/*.txt.gz; do ln -sf $$f `basename $$f .txt.gz`.txt.gz ; done

_webgraph_java:
        mkdir $@ ; cd $@ ;\
        curl -o deps.tar.gz http://webgraph.di.unimi.it/webgraph-deps.tar.gz;\
        tar zxvf deps.tar.gz ; rm -f deps.tar.gz;\
        curl -o webgraph3.tgz http://webgraph.di.unimi.it/webgraph-3.6.1-bin.tar
.gz;\
        tar zxvf webgraph3.tgz; rm -f webgraph3.tgz;\
        mv webgraph-3.6.1/webgraph-3.6.1.jar .


# add : uk-2002
# note:  fb-2011 (n=500m, m=100g)


dimacs9:
        for f in FLA CAL USA ; do \
                curl -o $(MASS)/USA-road-d.$$f.gr.gz http://www.dis.uniroma1.it/challenge9/data/USA-road-d/USA-road-d.$$f.gr.gz ; sleep 1;\
                curl -o $(MASS)//USA-road-d.$$f.co.gz http://www.dis.uniroma1.it/challenge9/data/USA-road-d/USA-road-d.$$f.co.gz ; sleep 1;\
                curl -o $(MASS)/USA-road-t.$$f.gr.gz http://www.dis.uniroma1.it/challenge9/data/USA-road-t/USA-road-t.$$f.gr.gz ; sleep 1;\
        done
        cd $(MASS) ; for f in *.gr.gz ; do name=`echo $$f | sed -e 's/USA-road-//'`; echo $name; ln -s $$f `basename $name .gr.gz`.txt.gz ; done


wargrids:
        curl -o $(MASS)/TheFrozenSea.zip http://movingai.com/benchmarks/sc1/TheFrozenSea.zip
        cd $(MASS) ; unzip TheFrozenSea.zip ; rm -f TheFrozenSea.zip
        ls $(MASS)/TheFrozenSea.map

# there is a frozensea in starcraft...

# 3D mesh data http://graphics.stanford.edu/data/3Dscanrep/


buddha:
        curl -o $(MASS)/happy_recon.tar.gz http://graphics.stanford.edu/pub/3Dscanrep/happy/happy_recon.tar.gz
        cd $(MASS) ; tar xvf happy_recon.tar.gz happy_recon/happy_vrip.ply
        mv $(MASS)/happy_recon/happy_vrip.ply $(MASS)/
        rm -fr $(MASS)/happy_recon* ;
        ls $(MASS)/happy_vrip.ply

vlsi:
        curl -o $(MASS)/ALUE.tgz http://steinlib.zib.de/download/ALUE.tgz
        cd $(MASS) ; tar xvf ALUE.tgz ALUE/alue7065.stp
        cd $(MASS) ; grep -e '^E ' ALUE/alue7065.stp | awk '{print $2,$3}' | gzip -c > alue7065.txt.gz
        ls $(MASS)/alue7065.txt.gz



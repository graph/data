
ALL:=$(wildcard massive/* snap/* sources_graphs/*)

help:
	@echo "'make all' to convert all graphs into undirected simple graphs\
		that are stored in '_simple'."
	@echo "'make sel' for a selection of 25 graphs."

all: $(patsubst %,_simple/%,$(ALL))


_graph/%: %
	@mkdir -p `dirname $@`
	cat $< | sed -e 's/^a //' -e 's/^[cp]/#/' -e 's/|/ /g' \
	| tr "\011" " " > $@

_simple/%: % _sug
	@mkdir -p `dirname $@`
	$(eval TMP:=$(shell mktemp -u _simple.XXXX))
	cat $< | sed -e 's/^a //' -e 's/^[cp]/#/' -e 's/|/ /g' \
	| tr "\011" " " | awk '{print($$1,$$2)}' | ./_sug 10 > $(TMP)
	mv $(TMP) $@

_sug: src/simple_undir_graph.rs
	rustc -o $@ $<


clean:
	rm -fr _*


SEL:=BIOGRID-MV-Physical-3.5 BIOGRID-SYSTEM-Affinity_Capture-MS-3.5 BIOGRID-SYSTEM-Affinity_Capture-RNA-3.5 dip20170205 \
oregon2_010331 CAIDA_as_20130601 DIMES_201204 as-skitter \
p2p-Gnutella09 gnutella31 notreDame y-BerkStan \
ca-HepPh com-dblp epinions1 facebook_combined twitter_combined \
t.CAL t.FLA buddha froz z-alue7065 \
grid300-10 xgrid500-10 powerlaw2.5

ALL_SEL:=$(shell echo $(ALL) | tr " " "\012" \
	   | grep -E "(`echo $(SEL) | tr " " "|"`)")

sel: $(patsubst %,_simple/%,$(ALL_SEL))


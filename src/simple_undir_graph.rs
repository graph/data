use std::env;
use std::io::{self, BufRead, BufReader};
use std::collections::HashMap;

pub type Node = u64;

/// an edge
#[derive(PartialEq, Eq, PartialOrd, Ord)]
pub struct Edge {
    /// tail
    pub u: Node,
    /// head
    pub v: Node,
}

fn main() {
    let args: Vec<String> = env::args().collect();
   
    if args.len() != 2 {
        panic!("\n\
Usage : {} [max_isolated_nodes]

Reads edges from stdin formatted as one per line as [u v] where [u] 
and [v] are two integers. [u v] and [v u] are interpreted indefferently. 
Lines beginning with # are ignored. The maximum number read [n] is interpreted 
as the total number of nodes.

Outputs a simple undirected graph (no multiple edges, no self loops) with one 
edge per line ([u v] with [u < v]). If there are more than [max_isolated_nodes]
isolated nodes (i.e. numbers less than [n] not appearing in any edge), then 
nodes are re-numbered from [1] to [n'] (by increasing numbers) so that there 
are no isolated nodes.

", &args[0]);
    }

    // -------------------- arguments -----------------------

    let max_isolated: u64 = args[1].parse().unwrap();
    

    // -------------------- read edges -----------------------

    let reader = BufReader::new(io::stdin());

    let mut edges: Vec<Edge> = reader.lines()
        .filter(|l| -> bool {
                let l = l.as_ref().unwrap();
                let l = l.trim();
                l.len() > 0 && ! l.starts_with('#')
        }).map(|l| -> Edge {
                let l = l.unwrap();
                let mut words = l.split_whitespace();
                let u: Node = parse_node(words.next()).unwrap();
                let v: Node = parse_node(words.next()).unwrap();
                if words.next() != None {
                   panic!("Too many numbers in the following line: {}", l);
                }
                if u <= v { Edge { u, v } } else { Edge { u: v, v: u } }
        }).collect();
    eprintln!("Read {} edges.", edges.len());


    // -------------------- inspect nodes -----------------------

    let mut nodes : Vec<Node> = Vec::with_capacity(2 * edges.len());
    for e in &edges {
        nodes.push(e.u);
        nodes.push(e.v);
    }   
    nodes.sort();
    nodes.dedup();
    let n_max : Node = match nodes.last() {
        Some(&n) => n,
        None => 0,
    };
    let n = nodes.len() as Node;
    eprintln!("Found {} nodes with maximal number {}.", n, n_max);

    let renumber = n_max > n + max_isolated;
    let mut nb = HashMap::<Node, Node>::new();
    if renumber {
       for (i, &v) in nodes.iter().enumerate() {
           nb.insert(v, (i+1) as Node);
       }
    }   


    // -------------------- output edges -----------------------

    edges.sort();
    edges.dedup();
    let mut m: u64 = 0;
    for e in &edges {
        if e.u != e.v { m += 1; }
    }

    println!("# Undirected graph with n={} nodes and m={} edges.", n, m);
    for Edge {u, v} in edges {
        if u != v {
           if renumber {
              println!("{} {}", nb[&u], nb[&v]);
           } else {
              println!("{} {}", u, v);
           }  
        }
    }
    eprintln!("Output {} edges.", m);
    
}

fn parse_node(opt: Option<&str>) -> Result<Node, &'static str> {
    match opt {
           Some(s) => return s.parse::<Node>().map_err(|_| "bad number format"),
           None => return Err("number expected"),
    }
}
    


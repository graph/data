# Graph data

This project gather some graphs coming from:

 * [SNAP @ Stanford University](http://snap.stanford.edu/)
 * [WEBGRAPH @ Milano University](http://webgraph.di.unimi.it/)
 * [DIMACS9 @ Roma University](http://www.dis.uniroma1.it/challenge9)
 * [SOCIALNETWORKS @ MPI-SWS](http://socialnetworks.mpi-sws.org/datasets.html)
 * [SteinLib @ Zuse Institute Berlin](https://steinlib.zib.de/steinlib.php) : VLSI graphs (alue7065)
 * [Moving AI Lab](https://movingai.com/) : computer games (FrozenSea, AR0503SR)


This includes graphs mentionned in :

 * Daniel Delling, Andrew V. Goldberg, Thomas Pajor, Renato F. Werneck. [Robust Distance Queries on Massive Networks.](https://www.microsoft.com/en-us/research/wp-content/uploads/2014/07/complexTR-rev2.pdf) (ESA'2014) (`massive` directory plus `snap/as-skitter.txt`)
 * David Coudert, André Nusser, Laurent Viennot. [Enumeration of Far-Apart Pairs by Decreasing Distance for Faster Hyperbolicity Computation](https://arxiv.org/abs/2104.12523) (J. Exp. Alg. 2022) [Hyperbolicity Computation through Dominating Sets.](https://arxiv.org/abs/2111.08520) (ALENEX 2022) 

## Prepare graphs

Most files are originals, simple undirected versions can be obtained into directory `_simple` with the following command (requires rust to be installed):

```
make all
```

A selection of 25 graphs can be obtained with:

```
make sel
```

